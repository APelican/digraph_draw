'''
A node is active if it is reached in a aktive step.
an aktive stepp is a step on an exising arrow.

'''

from digraph_draw.help_function.util import form_to_set_index
from copy import deepcopy


def reorientate_node(graph, edge, aktive, value_of_new_node, akt_working_set, pas_working_set):
    '''

    sets the prdecesor according to the edge

    updates the children of the onld predecessor,

    '''


    from_node = graph.nodes[edge[0]]
    to_node = graph.nodes[edge[1]]
    if aktive:
        #set new Value
        to_node.path_atr.akt.value = value_of_new_node

        # remove old path connection
        pred = to_node.path_atr.akt.pred
        graph.nodes[pred].path_atr.akt.children.remove(to_node.id)

        # add new path connection
        to_node.path_atr.akt.pred = from_node.id
        from_node.path_atr.akt.children.add(to_node.id)


        # readust working set
        pas_working_set.add(to_node.id)
    else:
        # set new Value
        to_node.path_atr.pas.value = value_of_new_node

        # remove old path connection
        pred = to_node.path_atr.pas.pred
        graph.nodes[pred].path_atr.pas.children.remove(to_node.id)

        # add new path connection
        to_node.path_atr.pas.pred = from_node.id
        from_node.path_atr.pas.children.add(to_node.id)

        # readjust working set
        akt_working_set.add(to_node.id)
    adjust_cross_surplus_array(graph, edge, aktive)



def set_node_path_properties(graph, edge, aktive):
    ''' auments the childeren of from node, sets the predsecesor and value of to node '''
    to_node = graph.nodes[edge[1]]
    from_node = graph.nodes[edge[0]]

    if aktive:
        from_node.path_atr.akt.children.add(to_node.id)
        to_node.path_atr.akt.pred = from_node.id
        to_node.path_atr.akt.value = from_node.path_atr.pas.value + value_change_edge(graph, edge, aktive)
    else:
        from_node.path_atr.pas.children.add(to_node.id)
        to_node.path_atr.pas.pred = from_node.id
        to_node.path_atr.pas.value = from_node.path_atr.akt.value + value_change_edge(graph, edge, aktive)
    adjust_cross_surplus_array(graph, edge, aktive)

def clear_node_path_atributes(graph):

    for node in graph.nodes:
        # aktive
        node.path_atr.akt.pred = None
        node.path_atr.akt.children = set()
        node.path_atr.akt.value = None
        node.path_atr.akt.crossing_surplus_array = None

        # passive
        node.path_atr.pas.pred = None
        node.path_atr.pas.children = set()
        node.path_atr.pas.value = None
        node.path_atr.pas.crossing_surplus_array = None



def value_change_edge(graph, edge, is_deliting):

    ''' Check wether adding / deliting would agrre with the Cross adge restrictions, here is taken care that the insertion of
    a passive (not deleting) edge would be in the reverse order'''
    # degrease is a boolean, indicating wheter you want to add or take away an arrow
    from_ind, to_ind = form_to_set_index(graph, edge)
    if from_ind==to_ind:
        return 0
    else:
        if is_deliting:
            crossing_surplus_array = graph.nodes[edge[0]].path_atr.pas.crossing_surplus_array
            if crossing_surplus_array[from_ind,to_ind] > 0:
                return 1
            else:
                return -1
        if not(is_deliting):
            crossing_surplus_array = graph.nodes[edge[0]].path_atr.akt.crossing_surplus_array
            # note reverse order of passive node
            if crossing_surplus_array[to_ind,from_ind] < 0:
                return 1
            else:
                return -1


def value_of_next_node(graph, edge, is_deliting):
    # here is takken care of revers order of passive edges in path
    if is_deliting:
        value_old_node = graph.nodes[edge[0]].path_atr.pas.value  # passive because has been reached in passive phase
        value_change = value_change_edge(graph, edge, is_deliting)
    else:
        value_old_node = graph.nodes[edge[0]].path_atr.akt.value
        value_change = value_change_edge(graph, edge, is_deliting)
    return value_old_node + value_change


def adjust_cross_surplus_array(graph, edge, aktive):
    ''' adusts the suplus array of the graphf if the edge is added (passive) subdrakted
    already taking care of direction swicht if added or subdracred'''

    # crosssurplus array of goal node has to be set first
    from_node = graph.nodes[edge[0]]
    to_node = graph.nodes[edge[1]]

    from_ind, to_ind = form_to_set_index(graph, edge)
    if aktive:
        new_crossing_surplus_array = deepcopy(from_node.path_atr.pas.crossing_surplus_array)
        if not (from_ind == to_ind):
            new_crossing_surplus_array[from_ind, to_ind] -= 1
        to_node.path_atr.akt.crossing_surplus_array = new_crossing_surplus_array

    else:
        new_crossing_surplus_array = deepcopy(from_node.path_atr.akt.crossing_surplus_array)
        # the not existing edges have to be added in reverse path order
        if not (from_ind == to_ind):
            new_crossing_surplus_array[to_ind, from_ind] += 1
        to_node.path_atr.pas.crossing_surplus_array = new_crossing_surplus_array



def found_illeagel_cycle(graph, node, aktive, check_aktive, check_node, pathorigin_node, pathorigin_node_aktive):
    '''
    Detects wheter a note to be reorienteated belongs to the current path
    (reorientation would lead to a ileagel cycle)

    works recursive, the goalnode is the node

    :return boolean of whether a ileagel cycle is detected
    '''

    if (node == check_node and aktive == check_aktive):
        return True


    if (node == pathorigin_node and aktive == pathorigin_node_aktive):
        return False

    if aktive:
        new_node = graph.nodes[node].path_atr.akt.pred
    else:
        new_node = graph.nodes[node].path_atr.pas.pred
    aktive = not(aktive)

    return found_illeagel_cycle(graph, new_node ,aktive, check_aktive, check_node, pathorigin_node, pathorigin_node_aktive)

