'''

Checks the feasablility of a input Degreesequenz:

Input:
  Graph: partially filled up sated with highest note with highest outdegree

Outut.
  Boolean (feasable/ not feasable)
  Graph: fully filed up or in case of not feasable None

Functionality

'''

# assumes that input sum of indegree and outdegree are 0
def check_degree_feasability(graph, from_node):
    while True:
        if graph.nodes[from_node].rsd_outdegree==0:
            from_node = max_outdegree_node(graph)
            # if all the edges has outdegee 0 then we found sucessfuly a graphrealisation
            if  graph.nodes[from_node].rsd_outdegree==0:
                return True, graph

        to_node = max_indegree_node(graph = graph,from_node=from_node)
        if to_node == None:
            return False, None
        else:
            graph.add_edge((from_node,to_node))


def max_indegree_node(graph,from_node):
    ''' findes to node (with maximal indegree and among them  max outdegree) for the from_node'''
    max_indegree = 0
    max_outdegree = 0
    to_node_max_indegree = None
    for to_node in range(graph.nodes.__len__()):
        if feasable_to_node(graph=graph,from_node=from_node, to_node=to_node):
            current_indegree = graph.nodes[to_node].rsd_indegree
            if current_indegree>max_indegree:
                max_indegree = graph.nodes[to_node].rsd_indegree
                max_outdegree =  graph.nodes[to_node].rsd_outdegree
                to_node_max_indegree = to_node
            if current_indegree==max_indegree and graph.nodes[to_node].rsd_outdegree> max_outdegree:
                max_indegree = graph.nodes[to_node].rsd_indegree
                max_outdegree = graph.nodes[to_node].rsd_outdegree
                to_node_max_indegree = to_node
    return to_node_max_indegree


def feasable_to_node(graph, from_node,to_node):
    if not(from_node  in graph.nodes[to_node].innodes) and not(from_node==to_node) and graph.nodes[to_node].rsd_indegree>0:
        return True
    else:
        return False



def max_outdegree_node(graph):
    max_outdegree = 0
    to_node_max_outdegree = None
    for node in range(graph.nodes.__len__()):
        if graph.nodes[node].rsd_outdegree >= max_outdegree:
                max_outdegree = graph.nodes[node].rsd_outdegree
                to_node_max_outdegree = node
    return to_node_max_outdegree

