from digraph_draw.help_function.util import edge_in_graph, cross_arrow_rst_check, value_change_edge
from digraph_draw.feasability.cycle_contructor_util import reorientate_node, set_node_path_properties,\
    value_of_next_node, clear_node_path_atributes, adjust_cross_surplus_array, found_illeagel_cycle


class Cycle_constructor():
    # implements jikarta like shortes path algoritm for cicle, considering aktive passive and the additional constraints

    def __init__(self, graph , edge, aktive):
        self.start_node = edge[1]
        self.goal_node = edge[0]
        self.graph = graph
        self.initial_edge = edge
        self.found = False


        self.active_woking_set = set()
        self.passive_woking_set = set()

        self.passive_open = set()
        for x in range(graph.node_number):
            if self.graph.nodes[x].max_outdegree > 0:
                self.passive_open.add(x)

        if aktive:
            # active is a step where only existing arrows are looked for
            self.arrive_active = False
            self.active = False
            self.passive_woking_set.add(edge[1])
            self.graph.nodes[edge[1]].path_atr.akt.value = 1
            self.graph.nodes[edge[0]].path_atr.pas.crossing_surplus_array = self.graph.crossing_surplus_array
            adjust_cross_surplus_array(self.graph, edge, True)

            #self.graph.crossing_surplus_array
        else:
            self.arrive_active = True
            self.active = True
            self.active_woking_set.add(edge[1])
            self.graph.nodes[edge[1]].path_atr.pas.value = 1
            self.graph.nodes[edge[0]].path_atr.akt.crossing_surplus_array = self.graph.crossing_surplus_array
            adjust_cross_surplus_array(self.graph, edge, False)


    def find_cycle(self):
        # precheck necesety to look at edge
        if self.check_initial_edge():
            clear_node_path_atributes(graph=self.graph)
            return False, None, None

        aumenting_phase = True
        while aumenting_phase:
            aumenting_phase = self.aument_nodes()

        if self.found:
            path = self.make_path()
            clear_node_path_atributes(graph=self.graph) # prinziple of having no side effects on the graph
            return True, path, self.arrive_active
        else:
            clear_node_path_atributes(graph=self.graph) # prinziple of having no side effects on the graph
            return False, None, None





    def aument_nodes(self):
        ''' loops through all active/passive working nodes and aments '''

        if self.active:
            working_set = self.active_woking_set
        else:
            working_set = self.passive_woking_set

        if working_set == set() or self.found:
            return False

        for node in working_set:
            self.aument_node(node)
        working_set.clear()

        self.active = not(self.active) # swich phase
        return True


    def aument_node(self, node):
        ''' finds all  to nodes for a node '''

        out_node_list = self.node_iterator(node)
        for out_node in out_node_list:
            edge = (node, out_node)
            if self.check_edge(edge):
                self.append_edge(edge)



    def node_iterator(self,node):
        """ creates a list of nodes which can be reached from the node in the phase"""
        outlist = []
        if self.active:
            for out_node in self.graph.nodes[node].outnodes:
                outlist.append(out_node)
        else:
            for out_node in self.passive_open:
                if not(out_node in self.graph.nodes[node].innodes): # would be replaced with arrow in oposit direction, Doublearrowrestriction
                        outlist.append(out_node)
        return outlist

    def check_edge(self, edge):

        """ Checks whether not reversion of first edge,  found goalnode"""
        if edge[0]==edge[1]:
            return False

        '''
        check of already visited but lover equal value in append edge handled/ refactor ! 
        '''
        if edge[1] == self.start_node and not(self.active == self.arrive_active):
            return False

        if self.active:
            if edge[0] == self.graph.working_node and edge[1] in self.graph.fixed_to_nodes:
                return False

        # check arrived
        if edge[1]==self.goal_node and self.active==self.arrive_active:
            # here ckeck whether the arrived has more value then 0 if not return false
            if value_of_next_node(graph=self.graph, edge=edge, is_deliting=self.active) > 0:
                self.found = True
            else:
                return False

        return True

    def append_edge(self, edge):
        '''updates working set, predecsesors, passive_open_set'''

        # preparation
        to_node = self.graph.nodes[edge[1]]
        if self.active:
            to_node_value = to_node.path_atr.akt.value
        else:
            to_node_value = to_node.path_atr.pas.value

        # case: Unmarked
        if to_node_value==None:
            # handle not marked node
            set_node_path_properties(graph=self.graph, edge=edge, aktive=self.active)

            if self.active:
                self.passive_woking_set.add(edge[1])
            else:
                self.active_woking_set.add(edge[1])

        # case: Marked
        else:
            value_of_new_node = value_of_next_node(self.graph, edge, is_deliting = self.active)
            if value_of_new_node > to_node_value:
                if not(found_illeagel_cycle(graph=self.graph, node=edge[0], aktive= not(self.active), check_node= to_node.id,
                                            check_aktive= self.active, pathorigin_node=self.start_node,
                                            pathorigin_node_aktive= not(self.arrive_active))):

                    reorientate_node(self.graph, edge, self.active, value_of_new_node,
                                     akt_working_set=self.active_woking_set,pas_working_set=self.passive_woking_set)



    def check_initial_edge(self):
        aktive_ititial_edge = not(self.active) # curret active is reffereing to the arrow which is to be added

        if aktive_ititial_edge:
            edge = (self.goal_node, self.start_node)
        else:
            edge = (self.start_node, self.goal_node) # the passie edges will be inserted in reversed order
        is_deliting = aktive_ititial_edge # active edges will be deleted

        return not(cross_arrow_rst_check(self.graph, edge, is_deliting))


    '''path creation after found'''
    def make_path(self):
        path = []
        self.active = self.arrive_active
        self.aument_path(path, self.goal_node)
        return list(reversed(path))

    def aument_path(self, path, node_id):
        if node_id == self.start_node and not(self.arrive_active == self.active):
            path.append((self.goal_node, self.start_node))
        else:
            node = self.graph.nodes[node_id]
            if self.active:
                predececor_node = node.path_atr.akt.pred
            else:
                predececor_node = node.path_atr.pas.pred
            self.active = not (self.active)  # swich phase
            path.append((predececor_node, node_id))
            self.aument_path(path, predececor_node)
