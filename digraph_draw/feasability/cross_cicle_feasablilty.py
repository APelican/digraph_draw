'''
finds the cicle which crosses border of the partitions set

Two cases
* aumentation : inserts a crossarrow
* reduction : takes a crossarrow away


sets are always looked from the lover setnumber to the bigger (reverse direction is taken care of by piginwhole principle)

proceedure:
1) find a potential critical crossparrow (iterable, over them)
    * aumentation one to be insertet (who has at the form node an positive indegree/ at the to age a positive outdegree)
    * reduction an existing arrow crossing the boarder

2) search for a path for each critical arrow:
    * altering path search,

    - Dijkstra kind algoritmus
    - aktive edge : edge which exists
    - passive edge : edge with no contra arrow
    node : aktiv value : value reached in aktiv step
           aktiv crossing_surplus_array : surplus_array reached in aktiv step
           aktive pred : predecessor over aktive edge
           aktive children: childrer over aktive edges
           aktive working_set : set of nodes, from which an aktive arrow leaves (arrived via a passive arrow)



   repeate until found

3) if found make a cycle swap,
   if not found return false.




itarable over critical crossarrows:

Reduction:


today only crossarow reduction implementation:
'''

''' for development    
from digraph_draw.graph import RstGraph
graph = RstGraph()
'''


from digraph_draw.feasability.cycle_constructor import Cycle_constructor
from digraph_draw.help_function.util import form_to_set_index, edge_in_graph, all_edges
from digraph_draw.help_function.control_functions import full_graph_correct, graph_correct, graph_is_full
from digraph_draw.feasability.cycle_contructor_util import adjust_cross_surplus_array
''' Main: '''

# fixme: make dinamic iterator over all potential aumenting sepps:


def cross_cicle_feasablility(rstGraph):
    for i in range(5):
        correct_aum_crossarrow(rstGraph) # Graph is modified
        correct_red_crossarrow(rstGraph) # Graph is modified


    # chere check if graph is correct:
    try:
        # inefficient big test
        full_graph_correct(graph=rstGraph)
        return True
    except:
        return False


''' AUMENTATION: '''

def correct_aum_crossarrow(rstGraph):
    pod_aum_edges_list = pod_aum_edges(rstGraph)
    for edge in pod_aum_edges_list:
        cycle_constructor = Cycle_constructor(graph = rstGraph, edge=edge, aktive= False)
        found, cycle , arrive_active = cycle_constructor.find_cycle()
        #inefficient
        rstGraph.update_crossing_surplus_array()
        if found:
            path_swich(rstGraph,cycle, arrive_active)



# fixme: inefficient
def pod_aum_edges(rstGraph):
    pod_aum_edges_list = []
    for from_node in range(rstGraph.node_number):
        for to_node in range(rstGraph.node_number):
            from_ind, to_ind = form_to_set_index(graph=rstGraph, edge=(from_node, to_node))
            if rstGraph.crossing_surplus_array[from_ind,to_ind] < 0:
                # check wheter oposite edge does not exists!
                if not(edge_in_graph((from_node,to_node), rstGraph)):
                    pod_aum_edges_list.append( (to_node,from_node))
    return pod_aum_edges_list

''' REDUCTION:'''
# fixme: inefficient
def pod_red_edges(rstGraph):
    pod_red_edges_list = []
    for edge in all_edges(rstGraph):
        from_ind, to_ind = form_to_set_index(graph=rstGraph, edge=(edge[0], edge[1]))
        if rstGraph.crossing_surplus_array[from_ind, to_ind] > 0:
            pod_red_edges_list.append(edge)
    return pod_red_edges_list

def correct_red_crossarrow(graph):
    pod_red_edges_list = pod_red_edges(graph)
    for edge in pod_red_edges_list:
        graph_correct(graph)
        graph_is_full(graph)
        cycle_constructor = Cycle_constructor(graph = graph, edge=edge,  aktive= True)
        found, cycle, arrive_active = cycle_constructor.find_cycle()
        graph.update_crossing_surplus_array()

        if found:
            path_swich(graph,cycle, arrive_active)
            graph.update_crossing_surplus_array()






''' GENERAL'''


def path_swich(graph, path, arrive_active):
    ''' siwches the altering path, and updates the set restrictions '''
    aktive = not(arrive_active)
    for edge in path:
        if aktive:
            graph.del_edge(edge)
        else:
            graph.add_edge((edge[1],edge[0]))
        aktive = not(aktive)
    graph.update_crossing_surplus_array()



