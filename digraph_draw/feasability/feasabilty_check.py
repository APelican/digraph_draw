'''
Copies the Graph
performs
degree_feasability, crosscycle_feasability on the copy.

returns true in case the Graph is feasable
'''
from digraph_draw.help_function.util import edge_in_graph, form_to_set_index
from digraph_draw.feasability.cross_cicle_feasablilty import cross_cicle_feasablility
from digraph_draw.feasability.degree_feasability import check_degree_feasability
from digraph_draw.help_function.control_functions import full_graph_correct, graph_correct
from copy import deepcopy


def is_feasable(graph_original, from_node, to_node):
    graph_original.update_crossing_surplus_array()
    if not(preconditions_check(graph_original, from_node, to_node)):
        return False
    # preperation, Graphcopy ect:
    graph = create_reduction_graph(graph_original, from_node, to_node)
    graph_correct(graph)

    # degree check
    is_degreefeasable, graph = check_degree_feasability(graph=graph,from_node=from_node)

    if not (is_degreefeasable):
        return False
    graph.update_crossing_surplus_array()
    graph_correct(graph)

    # transfromation for border check

    # crossing check
    if cross_cicle_feasablility(graph):
        return True
    else:
        return False


# assumes arrows are added outnode by outnode
def create_reduction_graph(graph_original, from_node, to_node):
    graph = deepcopy(graph_original)
    graph.add_edge((from_node, to_node))
    graph.working_node = from_node
    graph.fixed_to_nodes = deepcopy(graph.nodes[from_node].outnodes)

    for node_id in range(graph.node_number):
        if not(node_id==from_node):
            outnode_set = deepcopy(graph.nodes[node_id].outnodes)
            for outnode in outnode_set:
                graph.del_edge((node_id,outnode))
                graph.nodes[node_id].max_outdegree -= 1
                graph.nodes[node_id].rsd_outdegree -= 1
                graph.outdegree_serie[node_id] -= 1
                graph.nodes[outnode].max_indegree -= 1
                graph.nodes[outnode].rsd_indegree -= 1
                graph.indegree_serie[outnode] -= 1
                from_ind, to_ind = form_to_set_index(graph, (node_id,outnode))
                if not(from_ind == to_ind):
                    graph.crossing_np_array[from_ind, to_ind] -= 1

            #fixme: here also create take care of array_np and write test case for it
    return graph


def preconditions_check(graph, from_node, to_node):
    edge =(from_node, to_node)
    if from_node==to_node:
        return False

    if edge_in_graph((from_node, to_node), graph):
        return False

    if graph.nodes[to_node].rsd_indegree<= 0:
        return False
    # oudegree should not be checked (is handeled in draw algortimus)

    from_ind, to_ind =  form_to_set_index(graph, edge)
    if not(from_ind == to_ind):
        if graph.crossing_surplus_array[from_ind,to_ind] >= 0:
            return False
    return True






