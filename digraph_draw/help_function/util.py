
def form_to_set_index(graph, edge):
    for ind, set in enumerate(graph.restriction_set_list):
        if edge[0] in set:
            from_ind = ind
        if edge[1] in set:
            to_ind = ind
    return from_ind, to_ind

def cross_arrow_rst_check(graph, edge, is_deliting):
    ''' Check wether adding / deliting would agrre with the Cross adge restrictions'''
    # degrease is a boolean, indicating wheter you want to add or take away an arrow
    from_ind, to_ind = form_to_set_index(graph, edge)
    if from_ind==to_ind:
        return True
    else:
        if graph.crossing_surplus_array[from_ind,to_ind] > 0:
            return is_deliting
        if graph.crossing_surplus_array[from_ind,to_ind] < 0:
            return not(is_deliting)
        if graph.crossing_surplus_array[from_ind, to_ind]==0:
            return False

def value_change_edge(graph, edge, is_deliting):
    ''' Check wether adding / deliting would agrre with the Cross adge restrictions'''
    # degrease is a boolean, indicating wheter you want to add or take away an arrow
    from_ind, to_ind = form_to_set_index(graph, edge)
    if from_ind==to_ind:
        return 0
    else:
        if is_deliting:
            if graph.crossing_surplus_array[from_ind,to_ind] > 0:
                return 1
            else:
                return -1
        if not(is_deliting):
            if graph.crossing_surplus_array[from_ind,to_ind] < 0:
                return 1
            else:
                return -1
        if graph.crossing_surplus_array[from_ind, to_ind]==0:
            return -1

def edge_in_graph(edge, graph):
    return edge[1] in graph.nodes[edge[0]].outnodes

def all_edges(graph):
    edges = []
    for from_node in range(graph.node_number):
        for to_node in graph.nodes[from_node].outnodes:
            edges.append((from_node,to_node))
    return edges

def empty_graph(graph):
    for edge in all_edges(graph):
        graph.del_edge(edge)
        from_id, to_id = form_to_set_index(graph,edge)
        if not(from_id==to_id):
            graph.crossing_np_array[from_id, to_id] -= 1