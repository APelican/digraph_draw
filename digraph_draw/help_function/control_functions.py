'''
conrol functions:
Performing checks on the Graph, to see whether implicit restrictions (dopple arrow, degree  ect).
'''


'''  
from digraph_draw.graph import RstGraph,Graph, Node
graph=Graph()
node = Node()

rstrGraph = RstGraph
'''
import numpy as np
from digraph_draw.help_function.util import all_edges, form_to_set_index


def graph_correct(graph):
    restriction(graph)
    graph_nodes_correct(graph)
    return True

def full_graph_correct(graph):
    restriction(graph)
    graph_nodes_correct(graph)
    graph_is_full(graph)
    matrix_consistent(graph)
    return True

def graph_nodes_correct(graph):
    node_id_in_place(graph)
    for node in graph.nodes:
        node_correct(node)
    return True


def restriction(rstrGraph):
    set_list = rstrGraph.restriction_set_list
    total_Set = set()
    total = 0
    for sub_set in set_list:
        total_Set= total_Set.union(sub_set)
        total += sub_set.__len__()
    if not(total==total_Set.__len__()):
        raise ValueError ('restriction sets are not disjoint')
    if not(total_Set == set(range(rstrGraph.outdegree_serie.__len__()))):
        raise ValueError ('nodes in set are not the intergers {0,..n-1}')

def graph_is_full(graph):
    for i, node in enumerate(graph.nodes):
        if not (graph.indegree_serie[i] == node.max_indegree) or not (node.rsd_indegree == 0) or not (
            node.max_indegree == node.innodes.__len__()):
            raise ValueError('indegree property not fulfielled')
        if not (graph.outdegree_serie[i] == node.max_outdegree) or not (node.rsd_outdegree == 0) or not (
            node.max_outdegree == node.outnodes.__len__()):
            raise ValueError('indegree property not fulfielled')

def matrix_consistent(rstGraph):
    n = rstGraph.restriction_set_list.__len__()
    controll_np_array = np.zeros((n,n))
    for edge in all_edges(rstGraph):
        from_ind, to_ind = form_to_set_index(rstGraph, edge)
        if not(from_ind==to_ind):
            controll_np_array[from_ind,to_ind] +=1
    if not(controll_np_array == rstGraph.crossing_np_array).all():
        raise ValueError('Graph has the setrestricions violated')





'''Node'''
def node_id_in_place(graph):
    for i, node in enumerate(graph.nodes):
        if not(i==node.id):
            raise ValueError ('id and node position does not match')

def node_correct(node):
    for outnode in node.outnodes:
        if outnode == node.id:
            raise ValueError ('self loop')
    for innodee in node.innodes:
        if innodee == node.id:
            raise ValueError ('self loop')
    if not(node.max_outdegree-node.rsd_outdegree == node.outnodes.__len__()):
        raise ValueError ('resdual degree is wrong!')
    if not(node.max_indegree-node.rsd_indegree == node.innodes.__len__()):
        raise ValueError ('resdual degree is wrong!')

'''Subgraph'''

def is_subgraph(graph, subgraph):
    ''' check wether arrows of the input graph in the isfeasable function are not modified by the cross_cycle part'''
    pass