class Node():
    def __init__(self, id, max_indegree, max_outdegree):
        self.id = id
        self.max_indegree = max_indegree
        self.rsd_indegree = max_indegree
        self.max_outdegree = max_outdegree
        self.rsd_outdegree = max_outdegree
        self.innodes = set()
        self.outnodes = set()

        ''' Atributes for cycle search'''
        self.path_atr = Path_atributes()



    def add_outarrow(self, out_node):
        self.rsd_outdegree -= 1
        self.outnodes.add(out_node)

    def del_outarrow(self, out_node):
        if not(out_node in self.outnodes) or self.rsd_outdegree == self.max_outdegree:
            raise ValueError('edge subtraction error')
        else:
            self.rsd_outdegree += 1
            self.outnodes.remove(out_node)

    def add_inarrow(self, in_node):
        self.rsd_indegree -= 1
        self.innodes.add(in_node)

    def del_inarrow(self, in_node):
        if not (in_node in self.innodes) or self.rsd_indegree == self.max_indegree:
            raise ValueError('edge subtraction error')
        else:
            self.rsd_indegree += 1
            self.innodes.remove(in_node)

class Path_atributes():
    def __init__(self):
        self.akt = Akt_pas()
        self.pas = Akt_pas()

class Akt_pas():
    def __init__(self):
        self.children = set()
        self.pred = None
        self.value = None
        self.crossing_surplus_array = None

