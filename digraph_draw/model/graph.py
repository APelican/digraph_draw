from digraph_draw.help_function.util import form_to_set_index
from digraph_draw.model.node import Node
import copy


class Graph():
    # Graph class ( Constructed fot CPP implementation)
    # the Graph has n nodes {0,..,n-1}, the Node n is looked at and treated as NONE (such that in C implementation working with primitive types is possible)
    def __init__(self, indegree_serie, outdegree_serie):
        self.indegree_serie = indegree_serie
        self.outdegree_serie = outdegree_serie
        self.node_number = outdegree_serie.__len__()

        # for C implementation it
        self.nodes = []

        for i in range(indegree_serie.__len__()):
            self.nodes.append(Node(i,indegree_serie[i],outdegree_serie[i]))

    def add_edge(self, edge):
        from_id, to_id = edge
        from_node = self.nodes[from_id]
        from_node.add_outarrow(to_id)
        to_node = self.nodes[to_id]
        to_node.add_inarrow(from_id)

    def del_edge(self, edge):
        from_id, to_id = edge
        from_node = self.nodes[from_id]
        from_node.del_outarrow(to_id)
        to_node = self.nodes[to_id]
        to_node.del_inarrow(from_id)

    def inter_edges(self):
        for node in self.nodes:
            for outnode in node.outnodes:
                yield (node.id,outnode)



class RstGraph(Graph):
    # Graph extention with restriction:
    def __init__(self, indegree_serie, outdegree_serie, restriction_set_list, crossing_np_array):
        self.restriction_set_list = restriction_set_list # list of the restriction subsets, disjunct nodepartition
        self.crossing_np_array = crossing_np_array
        # np array of integers indexed according to (from_set, to_set). Diagonals 0 (implicit given by setrestrictions)
        # Validate if it fits to the degreesequenz ??
        self.crossing_surplus_array = None

        # fixed edges for cross_cycle algorithm are set in is_feasable algorimus
        self.working_node = None
        self.fixed_to_nodes = None
        # build and inserted after random draw from
        super(RstGraph, self).__init__(indegree_serie, outdegree_serie)





    def update_crossing_surplus_array(self):
        array = copy.deepcopy(self.crossing_np_array)
        self.crossing_surplus_array = - array
        for edge in self.inter_edges():
            from_ind, to_ind = form_to_set_index(self, edge)
            if not(from_ind==to_ind):
                self.crossing_surplus_array[from_ind,to_ind] +=1




