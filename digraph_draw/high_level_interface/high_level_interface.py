'''
Simulation DiGraphs:

Creating random directed graphs according to a in- and outdegreesequence.

The core implementations of the algorithm is written in C++, this is merely an interface
for convenient usage.

Algorithm is implemented according to the paper:
    Erzäugung von gerichtete Zufallgraphen mit gegebener eingangs und outganfs Sequenz
and an application in
    Strategic citing

The dependencies are:
    numpy
    os
    json

'''
import os
import numpy as np
import json

def gen_digraph(adj_m, anz_sim):
    '''
    Generates Random Digraphs with in- and outdegee equal to the input adjancy matrix

    :param adj_m:   2 dimensional numpy array with only 0 and 1 entries (0 on the diagonal) representing
                    an adjancy matrix.
           anz_sim: positive integer, number of output simulations.


    :return adj_m_list:  list of 2 dimensional numpy array represinting the simulated digraphs
    :return weight_list: a list of the graphweights, the weights are tuples of a (a,b) float and int,
                         representing the number a*10^b
    '''

    # input validation:

    if not(isinstance(adj_m, np.ndarray)):
        raise ValueError("matrix must be a numpy array")

    n = adj_m.shape[0]
    if not(adj_m.shape[0]==adj_m.shape[1]):
        ValueError(' matrix must be semetric')

    for i in range(n):
        for j in range(n):
            if not(adj_m[i,j] == 0) and not (adj_m[i,j]== 1):
                raise ValueError('matrix is only allowed to have 0, 1 entries (no double arrow)')

    for i in range(n):
        if adj_m[i, i] == 1:
            raise ValueError('diagonal entries of matrix must be 0, (no self loops)')

    try:
        anz_sim = int(anz_sim)
    except ValueError:
        print("anz sim must be a number")

    if anz_sim<= 0 :
        raise ValueError("anz_sim must be positive")

    # fixme: add validation for setrestriciton, and a defould variable for set restriction
    # fixme: add enter the right interfce to the draw part
    # Actual logic:
    n, indegrees, outdegrees = adj_to_in_out_sequences(adj_m)
    sdgCppModule.runDigraphSimulations(anz_sim, n, indegrees, outdegrees)
    return None






def adj_to_in_out_sequences(adj):
    n = adj.shape[0]
    indegrees = []
    outdegrees = []
    for i in range(n):
        indegrees.append(sum(adj[i,:]))
        outdegrees.append(sum(adj[:, i]))
    return n, indegrees, outdegrees



def list_to_adj(list_digraph_representation):
    n = list_digraph_representation.__len__()
    adj_m = np.zeros((n,n))
    for i in list_digraph_representation:
        for j in list_digraph_representation[i]:
            adj_m[int(i),j]=1;
    return adj_m;
