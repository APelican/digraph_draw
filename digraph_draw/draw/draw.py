'''
Implenents one random draw of a Graph with restiction (indegree-, outdegreeserie and Kotesetpartition with crossarrows)
'''

import numpy as np

from digraph_draw.feasability.feasabilty_check import is_feasable
from digraph_draw.feasability.degree_feasability import max_outdegree_node
from digraph_draw.model.graph import RstGraph

from digraph_draw.help_function.control_functions import graph_correct


def draw(indegree_serie,outdegree_serie, restriction_set_list, crossing_np_array):
    graph = RstGraph(indegree_serie, outdegree_serie,restriction_set_list,crossing_np_array )
    graph_correct(graph)

    to_node_max_outdegree = max_outdegree_node(graph)

    n_edgeseries_to_graph = 1
    sel_probaility = 1

    n_edgeseries_to_graph, sel_probaility = aumentRandomDraw(graph, working_node = to_node_max_outdegree, n_edgeseries_to_graph= n_edgeseries_to_graph, sel_probaility= sel_probaility)
    # A graph has a smaller weight if there are many ways to arrive at it,
    # and if the probability of one age sequence is low, for details see elaborations at the paper
    weight = 1/(sel_probaility *n_edgeseries_to_graph)
    # create a Simulation instance
    return graph, weight


''' Full implementation '''
# inefficient recursion
def aumentRandomDraw(graph, working_node, n_edgeseries_to_graph, sel_probaility):
    out_degree = graph.nodes[working_node].rsd_outdegree
    if out_degree == 0:
        working_node = max_outdegree_node(graph)
        out_degree = graph.nodes[working_node].rsd_outdegree

        if graph.nodes[working_node].rsd_outdegree == 0: # end of aumentation
            return n_edgeseries_to_graph, sel_probaility

    n_edgeseries_to_graph *= out_degree
    feasable_innodes_list = setOfFeasableInNodes(graph, working_node)

    # random draw according to heuristic
    weights =[]
    for innode in feasable_innodes_list:
        weights.append(graph.nodes[innode].rsd_indegree)
    weights = np.array(weights)
    weights = weights/weights.sum()
    help_list = list(range(0,feasable_innodes_list.__len__()))
    index = np.random.choice(help_list, p=weights)
    to_node = feasable_innodes_list[index]
    graph.add_edge((working_node,to_node))
    node_probability = weights[index]
    sel_probaility = sel_probaility*node_probability
    return aumentRandomDraw( graph, working_node, n_edgeseries_to_graph, sel_probaility)

def setOfFeasableInNodes(graph, working_node ):
    feasable_innodes = []
    for to_node in range(graph.node_number):
        if is_feasable(graph,from_node=working_node, to_node= to_node):
            feasable_innodes.append(to_node)
    if feasable_innodes==[]:
        raise ValueError('There in no Graph, with the input degreesequenzes and crossarrow restriction')
    return feasable_innodes