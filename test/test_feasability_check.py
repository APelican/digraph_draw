import copy
import pytest

from digraph_draw.feasability.feasabilty_check import is_feasable
from test.test_resources.input_more_restriction import *
from test.test_resources.citation_example_graph import graph_mr_cit_emty
from test.test_resources.graphs_two_restriction_sets import *




# empty graphs

testdata = [

    (graph_mr_1_emty, (5, 0), True),
    (graph_mr_1_emty, (5, 1), True),
    (graph5emty, (5, 7), True),
    (graph1emty, (3, 2), False),
    (graph1emty, (2, 3), False),
    (graph1emty, (2, 1), False),
    (graph1emty, (2, 2), False),
    (graph1emty, (2, 0), False),
    (graph2emty, (3, 2), False),
    (graph3emty, (2, 5), False),
    (graph3emty, (2, 3), True),
    (graph3emty, (4, 3), False),
    (graph4emty, (4, 3), False),
    (graph4emty, (3, 4), False),
    (graph4emty, (3, 4), False),
    (graph5emty, (5, 7), True),
    (graph5emty, (0, 2), False), # test fixed edge
    (graph_mr_1_emty, (5, 0), True),
    (graph_mr_1_emty, (5, 1), True),
    (graph_mr_0_emty, (0, 1), True),
    ]

@pytest.mark.parametrize("graph, edge, feasable", testdata)
def test_is_feasable(graph, edge, feasable):
    graph_copy = copy.deepcopy(graph)
    is_feasable_t = is_feasable(graph_copy, edge[0], edge[1])
    assert is_feasable_t == feasable


# test half filled up graphs
#graph5emty.add_edge((3,1))

testdata = [
    (graph5emty, (3,4), True),
    (graph5emty, (3,2), True),
    (graph5emty, (3,1), False),
    ]

@pytest.mark.parametrize("graph, edge, feasable", testdata)
def test_is_feasable2(graph, edge, feasable):
    graph_copy = copy.deepcopy(graph)
    graph_copy.add_edge((3,1))
    is_feasable_t = is_feasable(graph_copy, edge[0], edge[1])
    assert is_feasable_t == feasable


testdata = [
    (graph5emty, (3, 4), False),
    (graph5emty, (3,2),  False),
    (graph5emty, (3,1),  False),
    ]
# two edges already complete filled up
@pytest.mark.parametrize("graph, edge, feasable", testdata)
def test_is_feasable3(graph, edge, feasable):
    graph_copy = copy.deepcopy(graph)
    graph_copy.add_edge((3, 1))
    graph_copy.add_edge((7, 6))
    graph_copy.add_edge((6, 7))
    is_feasable_t = is_feasable(graph_copy, edge[0], edge[1])
    assert is_feasable_t == feasable




testdata = [
    (graph3emty, (4,7), True),
    (graph3emty, (0,1), False),
    ]
# with corssarrows needed and one in graph already
@pytest.mark.parametrize("graph, edge, feasable", testdata)
def test_is_feasable4(graph, edge, feasable):
    graph_copy = copy.deepcopy(graph)
    graph_copy.crossing_np_array = np.array(np.array(([0,1],[1,0])))
    graph_copy.add_edge((2, 5))
    graph_copy.add_edge((2, 1))
    is_feasable_t = is_feasable(graph_copy, edge[0], edge[1])
    assert is_feasable_t == feasable



testdata = [
    (graph3emty, (4,7), True),
    (graph3emty, (0,3), False),
    ]
# needs to aument tow arrow
@pytest.mark.parametrize("graph, edge, feasable", testdata)
def test_is_feasable5(graph, edge, feasable):
    graph_copy = copy.deepcopy(graph)
    graph_copy.crossing_np_array = np.array(np.array(([0,2],[2,0])))
    graph_copy.add_edge((2, 5))
    graph_copy.add_edge((2, 1))
    is_feasable_t = is_feasable(graph_copy, edge[0], edge[1])
    assert is_feasable_t == feasable


# test cytation example, Constructed due to inconsistency in estimation how many exists
# this asserts wheter the pot_aum_edge is not already in graph

testdata = [
    (graph_mr_cit_emty, (6,15), True)
    ]
# needs to aument tow arrow
@pytest.mark.parametrize("graph, edge, feasable", testdata)
def test_is_feasable_citation(graph, edge, feasable):
    graph_copy = graph
    #graph_copy = copy.deepcopy(graph)
    graph_copy.crossing_np_array = np.array(np.array(([0,3,4],[0,0,3],[0,0,0])))
    graph_copy.add_edge((6, 13))
    graph_copy.add_edge((6, 12))
    is_feasable_t = is_feasable(graph_copy, edge[0], edge[1])
    assert is_feasable_t == feasable
