import copy

import numpy as np
import pytest

from digraph_draw.feasability.cross_cicle_feasablilty import *
from digraph_draw.help_function.control_functions import full_graph_correct, graph_correct
from digraph_draw.model.graph import RstGraph


graph = RstGraph(indegree_serie=[1,0,1,0], outdegree_serie=[0,1,0,1],restriction_set_list=[set([0,3]),set([1,2])],crossing_np_array = np.array(([0,0],[0,0])))
graph.add_edge((1,0))
graph.add_edge((3,2))
graph.update_crossing_surplus_array()
graph_correct(graph)



testdata = [
    (graph),
    ]

@pytest.mark.parametrize("graph", testdata)
def test_correct_red_crossarrow(graph):
    graph_copy = copy.deepcopy(graph)
    correct_red_crossarrow(graph_copy)
    assert full_graph_correct(graph_copy)



@pytest.mark.parametrize("graph", testdata)
def test_path_switch(graph):
    path = [(1,0),(0,3),(3,2),(2,1)]
    graph_copy = copy.deepcopy(graph)
    path_swich(graph_copy, path, False)
    assert full_graph_correct(graph_copy)



