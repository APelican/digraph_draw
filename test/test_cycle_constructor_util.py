import copy
import pytest

from digraph_draw.feasability.cycle_contructor_util import value_change_edge
from test.test_resources.graphs_two_restriction_sets import *

for node in graph3.nodes:
    node.path_atr.akt.crossing_surplus_array = graph3.crossing_surplus_array
    node.path_atr.pas.crossing_surplus_array = graph3.crossing_surplus_array



# empty graphs

testdata = [

    (graph3, (2, 5), True, 1),
    (graph3, (2, 5), False, -1),
    (graph3, (2, 3), True, 0),
    (graph3, (4, 3), False, -1),
    ]

@pytest.mark.parametrize("graph, edge, is_deliting, change_value", testdata)
def test_value_change_edge(graph, edge,is_deliting, change_value):
    graph_copy = copy.deepcopy(graph)
    change_value_t =  value_change_edge(graph, edge, is_deliting)
    assert change_value_t == change_value

