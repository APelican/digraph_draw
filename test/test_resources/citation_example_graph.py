import numpy as np
from digraph_draw.model.graph import RstGraph

# long cycle           [0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8]
indegree_serie_cit   = [0,0,0,0,0,0,0,0,0,2,0,0,1,1,2,1,1,2,0]
outdegree_serie_cit  = [0,2,0,1,0,1,3,0,0,1,2,0,0,0,0,0,0,0,0]
restriction_set_list_cit = [set([0,1,2,3,4,5,6]),set([7,8,9,10,11,12]),set([13,14,15,16,17,18])]
crossing_np_array_mr_cit = np.array(([0,3,4],[0,0,3],[0,0,0]))
graph_mr_cit_emty = RstGraph(indegree_serie_cit, outdegree_serie_cit,restriction_set_list_cit,crossing_np_array_mr_cit)
