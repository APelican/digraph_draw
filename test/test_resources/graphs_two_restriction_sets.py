from digraph_draw.model.graph import RstGraph
import numpy as np
from digraph_draw.help_function.control_functions import graph_correct

# reduction cyrcle
graph1 = RstGraph(indegree_serie=[1, 0, 1, 0], outdegree_serie=[0, 1, 0, 1], restriction_set_list=[set([0, 3]), set([1, 2])], crossing_np_array = np.array(([0, 0], [0, 0])))
graph1emty = RstGraph(indegree_serie=[1, 0, 1, 0], outdegree_serie=[0, 1, 0, 1], restriction_set_list=[set([0, 3]), set([1, 2])], crossing_np_array = np.array(([0, 0], [0, 0])))
graph1.add_edge((1, 0))
graph1.add_edge((3, 2))
graph1.update_crossing_surplus_array()

# aumentation cyrcle
graph2 = RstGraph(indegree_serie=[1,0,1,0], outdegree_serie=[0,1,0,1],restriction_set_list=[set([0,1]),set([2,3])],crossing_np_array = np.array(([0,1],[1,0])))
graph2emty = RstGraph(indegree_serie=[1,0,1,0], outdegree_serie=[0,1,0,1],restriction_set_list=[set([0,1]),set([2,3])],crossing_np_array = np.array(([0,1],[1,0])))
graph2.add_edge((1,0))
graph2.add_edge((3,2))
graph2.update_crossing_surplus_array()
graph_correct(graph2)


# long cycle
indegree_serie =[0,1,0,2,0,2,0,1]
outdegree_serie=[1,0,2,0,2,0,1,0]
restriction_set_list=[set([0,1,2,3]),set([4,5,6,7])]
crossing_np_array = np.array(([0,0],[0,0]))
graph3 = RstGraph(indegree_serie, outdegree_serie,restriction_set_list,crossing_np_array )
graph3emty = RstGraph(indegree_serie, outdegree_serie,restriction_set_list,crossing_np_array )

graph3.add_edge((0,1))
graph3.add_edge((2,3))
graph3.add_edge((2,5))
graph3.add_edge((4,3))
graph3.add_edge((4,5))
graph3.add_edge((6,7))
graph3.update_crossing_surplus_array()
graph_correct(graph3)

# impossible long
indegree_serie =[2,0,2,1,1,2,0]
outdegree_serie=[1,1,0,3,2,0,1]
restriction_set_list=[set([0,1,2,3]),set([4,5,6])]
crossing_np_array = np.array(([0,0],[0,0]))
graph4 = RstGraph(indegree_serie, outdegree_serie,restriction_set_list,crossing_np_array )
graph4emty = RstGraph(indegree_serie, outdegree_serie,restriction_set_list,crossing_np_array )
graph4.add_edge((1,0))
graph4.add_edge((0,2))
graph4.add_edge((3,0))
graph4.add_edge((3,2))
graph4.add_edge((3,4))
graph4.add_edge((4,3))
graph4.add_edge((4,5))
graph4.add_edge((6,5))
graph4.update_crossing_surplus_array()
graph_correct(graph4)

# possible not equlibrated:
# long cycle
indegree_serie =[0,2,1,1,1,1,1,1]
outdegree_serie=[2,0,0,3,0,1,1,1]
restriction_set_list=[set([0,1,2,3,4]),set([5,6,7])]
crossing_np_array = np.array(([0,0],[0,0]))
graph5 = RstGraph(indegree_serie, outdegree_serie,restriction_set_list,crossing_np_array )
graph5emty = RstGraph(indegree_serie, outdegree_serie,restriction_set_list,crossing_np_array )
graph5.add_edge((0,1))
graph5.add_edge((0,2))
graph5.add_edge((3,1))
graph5.add_edge((3,4))
graph5.add_edge((3,5))
graph5.add_edge((5,3))
graph5.add_edge((6,7))
graph5.add_edge((7,6))
graph5.update_crossing_surplus_array()
graph_correct(graph5)
