import copy

import pytest
from digraph_draw.feasability.cycle_constructor import Cycle_constructor
from test.test_resources.graphs_two_restriction_sets import *
from test.test_resources.input_more_restriction import *




testdata = [
    (graph_mr_2, (1, 4), True, [(1, 4), (4, 3), (3, 5), (5, 4), (4, 1), (1, 0), (0, 2), (2, 1)]),  # test occupied edge
    (graph1, (3, 2), True, [(3, 2), (2, 1), (1, 0), (0, 3)]),
    (graph1, (2, 3), False, None),
    (graph1, (2, 1), True, [(2, 1), (1, 0), (0, 3), (3, 2)]),
    (graph1, (2, 2), False, None),
    (graph1, (2, 0), False, None),
    (graph2, (3, 2), True, [(3, 2), (2, 1), (1, 0), (0, 3)]),
    (graph3, (2, 5), True, [(2, 5), (5, 6), (6, 7), (7, 4), (4, 3), (3, 0), (0, 1), (1, 2)]),
    (graph3, (4, 3), True, [(4, 3), (3, 0), (0, 1), (1, 2), (2, 5), (5, 6), (6, 7), (7, 4)]),
    (graph4, (4,3), False, None),
    (graph4, (3, 4), False, None),
    (graph5, (5, 3), True, [(5, 3), (3, 0), (0, 2), (2, 3), (3, 5), (5, 7), (7, 6), (6, 5)]),
    (graph_mr_1, (4, 3), True, [(4, 3), (3, 1), (1, 5), (5, 0), (0, 1), (1, 4)]), # Atention this has two possible solutions:  [(4, 3), (3, 1), (1, 0), (0, 2), (2, 1), (1, 4)]
    (graph_mr_2, (1,4), True, [(1,4),(4,3),(3,5),(5,4),(4,1),(1,0),(0,2),(2,1)]) # test occupied edge
]


@pytest.mark.parametrize("graph, edge, feasable, cycle", testdata)
def test_path_constructor(graph, edge, feasable, cycle):
    graph_copy = copy.deepcopy(graph)
    cycle_constructor = Cycle_constructor(graph_copy, edge)
    feasable_t, cycle_t, arrive_active_t = cycle_constructor.find_cycle()
    if not(cycle_t==None):
        cycle_t = list(cycle_t)
    assert (feasable_t,cycle_t)==(feasable,cycle)

