from copy import deepcopy
import pytest

from digraph_draw.draw.draw import draw
from test.test_resources.input_two_restriction import *
from test.test_resources.input_more_restriction import *

# empty graphs
np.random.seed(seed=2)
testdata = [
    # fixme there is still a bug, sometimes this gives an error
    (indegree_serie_mr_1, outdegree_serie_mr_1, restriction_set_list_mr_1, crossing_np_array_mr_1),
    (indegree_serie_1, outdegree_serie_1, restriction_set_list_1, crossing_np_array_1),
    (indegree_serie_2, outdegree_serie_2, restriction_set_list_2, crossing_np_array_2),
    (indegree_serie_3, outdegree_serie_3, restriction_set_list_3, crossing_np_array_3),
    (indegree_serie_5, outdegree_serie_5, restriction_set_list_5, crossing_np_array_5),
    (indegree_serie_mr_0, outdegree_serie_mr_0, restriction_set_list_mr_0, crossing_np_array_mr_0),
    (indegree_serie_mr_1, outdegree_serie_mr_1, restriction_set_list_mr_1, crossing_np_array_mr_1),
]

@pytest.mark.parametrize("indegree_serie, outdegree_serie , restriction_set_list , crossing_np_array", testdata)
def test_draw(indegree_serie, outdegree_serie , restriction_set_list , crossing_np_array):
    indegree_serie_c = deepcopy(indegree_serie)
    outdegree_serie_c = deepcopy(outdegree_serie)
    restriction_set_list_c = deepcopy(restriction_set_list)
    crossing_np_array_c = deepcopy(crossing_np_array)

    rand_graph = draw(indegree_serie_c,outdegree_serie_c, restriction_set_list_c, crossing_np_array_c)
    assert graph_correct(rand_graph)





testdata = [
    (indegree_serie_4, outdegree_serie_4, restriction_set_list_4, crossing_np_array_4),
]

@pytest.mark.parametrize("indegree_serie, outdegree_serie , restriction_set_list , crossing_np_array", testdata)
def test_draw_corrupt_imput(indegree_serie, outdegree_serie , restriction_set_list , crossing_np_array):
    with pytest.raises(ValueError):
        indegree_serie_c = deepcopy(indegree_serie)
        outdegree_serie_c = deepcopy(outdegree_serie)
        restriction_set_list_c = deepcopy(restriction_set_list)
        crossing_np_array_c = deepcopy(crossing_np_array)
        rand_graph = draw(indegree_serie_c,outdegree_serie_c, restriction_set_list_c, crossing_np_array_c)
