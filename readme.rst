digraph_draw 
%%%%%%%%%%%%%%%%%%%%%%%

Implementation of a random draw of a digraph with given degree sequences and given arrow crossing.


First version:
===============

partition of the node set into subsets and a given number of cross-arrows (arrows from one subset into the other).

Only the Feasibility Check implemented.


Architecture:
==============

All the logic is implemented in the digraph_draw folder. it is divided into

* model
    containing the data models (appropriate Graph representation for the problem)
  
* high_level_intervace
    User interface with validation
    
* help_functions

* draw
    creation of a digraph with given degree series/without the cross arrow restrictions
    
* feasibility 
    Contains the logic for creating/ modifying a Graph in order to see whether there exists a feasible implementation
      * creation of a digraph with given degree series/without the ross arrow restrictions
      * Modification using search of altering cycles
    
  
Testing:
==================

In the testing folder, there are testing files for the different submodules. In general the development is test-based, however, there is not full coverage jet (especially user interface ect is not covered).

The tests are written according to the pytest framework. 


Todo
==============

* write a proper iterator over the potential edges (in order to save RAM), which stops then the graph is correct and considers edges which are introduced over cycle swap
* write setup.py





